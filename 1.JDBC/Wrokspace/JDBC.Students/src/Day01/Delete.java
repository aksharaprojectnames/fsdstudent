package Day01;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

// Delete Student Record
public class Delete {
    public static void main(String[] args) {

        Connection con = null;
        Statement stmt = null;

        try {
            // Prompt user for Student ID
            System.out.print("Enter Student Id: ");
            int id = new Scanner(System.in).nextInt();
            System.out.println();

            // Database connection details
            String url = "jdbc:mysql://localhost:3306/fsd57";
            String username = "root";
            String password = "root";

            // SQL query to delete student record
            String deleteQuery = "DELETE FROM student WHERE id = " + id;

            // Load JDBC driver and establish connection
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(url, username, password);

            // Create a statement
            stmt = con.createStatement();

            // Execute the delete query
            int result = stmt.executeUpdate(deleteQuery);

            if (result > 0) {
                System.out.println("Student Record Deleted");

                // Execute the select query to fetch the remaining data
                String selectQuery = "SELECT * FROM student";
                ResultSet rs = stmt.executeQuery(selectQuery);

                // Print the remaining student records
                while (rs.next()) {
                    System.out.println("id      : " + rs.getInt("id"));
                    System.out.println("name    : " + rs.getString("name"));
                    System.out.println("gender  : " + rs.getString("gender"));
                    System.out.println("email   : " + rs.getString("email"));
                    System.out.println("password: " + rs.getString("password") + "\n");
                }

                rs.close();
            } else {
                System.out.println("Failed to Delete the Student Record. No matching record found for ID: " + id);
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            System.out.println("Error: " + e.getMessage());
        } finally {
            try {
                // Close statement and connection in the finally block
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}