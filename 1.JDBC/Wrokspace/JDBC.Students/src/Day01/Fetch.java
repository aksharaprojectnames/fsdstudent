package Day01;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
public class Fetch {
//Fetch Employee Data based on empName: select * from employee where empName='Harsha'
	public static void main(String[] args) {

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

		System.out.print("Enter student Name: ");
		String name = new Scanner(System.in).next();
		System.out.println();
		
		String url = "jdbc:mysql://localhost:3306/fsd57";
		String query = "Select * from student where name = '" + name + "'";
		
		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, "root", "root");

			stmt = con.createStatement();
			rs = stmt.executeQuery(query);

			if (rs.next()) {
				do {
					System.out.println("id      : " + rs.getInt("id"));
                    System.out.println("name    : " + rs.getString("name"));
                    System.out.println("gender  : " + rs.getString("gender"));
                    System.out.println("email   : " + rs.getString("email"));
                    System.out.println("password: " + rs.getString("password") + "\n");
				} while (rs.next());
			} else {
				System.out.println("Student Record(s) Not Found!!!");
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
}