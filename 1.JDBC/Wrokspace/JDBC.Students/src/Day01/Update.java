package Day01;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

// Update Student Name, Gender, Email, Password
public class Update {
    public static void main(String[] args) {

        Connection con = null;
        Statement stmt = null;

        System.out.println("Enter student Id, Name, Gender, Email, Password");
        Scanner scan = new Scanner(System.in);
        int id = scan.nextInt();
        String name = scan.next();
        String gender = scan.next();
        String email = scan.next();
        String password = scan.next();
        System.out.println();

        String url = "jdbc:mysql://localhost:3306/fsd57";

        // Use the SET clause to specify the columns to be updated
        String updateQuery = "update student set " +
                "name = '" + name + "', gender = '" + gender + "', " +
                "email = '" + email + "', password = '" + password + "' " +
                "where id = " + id;

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(url, "root", "root");

            stmt = con.createStatement();
            int result = stmt.executeUpdate(updateQuery);

            if (result > 0) {
                System.out.println("Student Record Updated");

                // Execute the select query to fetch the updated data
                String selectQuery = "select * from student";
                ResultSet rs = stmt.executeQuery(selectQuery);

                while (rs.next()) {
                    System.out.println("id      : " + rs.getInt("id"));
                    System.out.println("name    : " + rs.getString("name"));
                    System.out.println("gender  : " + rs.getString("gender"));
                    System.out.println("email   : " + rs.getString("email"));
                    System.out.println("password: " + rs.getString("password") + "\n");
                }

                rs.close();
            } else {
                System.out.println("Failed to Update the Student Record!!!");
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}