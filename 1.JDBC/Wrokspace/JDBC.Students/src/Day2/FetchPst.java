package Day2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class FetchPst {
    // Fetch Student Data based on student Name: select * from student where name='Harsha'
    public static void main(String[] args) {

        Connection con = null;
        PreparedStatement pst = null;
        ResultSet rs = null;

        System.out.print("Enter student Id: ");
        String name = new Scanner(System.in).next();
        System.out.println();

        String url = "jdbc:mysql://localhost:3306/fsd57";
        String query = "SELECT * FROM student WHERE id = ?";

        try {

            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(url, "root", "root");

            pst = con.prepareStatement(query);
            pst.setString(1, name);

            rs = pst.executeQuery();

            if (rs.next()) {
                do {
                    System.out.println("id      : " + rs.getInt("id"));
                    System.out.println("name    : " + rs.getString("name"));
                    System.out.println("gender  : " + rs.getString("gender"));
                    System.out.println("email   : " + rs.getString("email"));
                    System.out.println("password: " + rs.getString("password") + "\n");
                } while (rs.next());
            } else {
                System.out.println("Student Record(s) Not Found!!!");
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}