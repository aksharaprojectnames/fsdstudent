package Day2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import javax.swing.text.Segment;



public class UpdatePst {
    public static void main(String[] args) {

        Connection con = null;
        PreparedStatement pstUpdate = null;
        Segment stmtSelect = null;

        System.out.println("Enter student Id and New password");
        Scanner scan = new Scanner(System.in);
        int id = scan.nextInt();
        String newPassword = scan.next();
        System.out.println();

        String url = "jdbc:mysql://localhost:3306/fsd57";

        String updateQuery = "UPDATE student SET password = ? WHERE id = ?";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(url, "root", "root");

            // Create a PreparedStatement for the update operation
            pstUpdate = con.prepareStatement(updateQuery);
            pstUpdate.setString(1, newPassword);
            pstUpdate.setInt(2, id);

            // Execute the update query
            int result = pstUpdate.executeUpdate();

            if (result > 0) {
                System.out.println("Student Record Updated");

                // Execute the select query to fetch the updated data
                String selectQuery = "SELECT * FROM student ";
                pstUpdate = con.prepareStatement(selectQuery);
//                pstUpdate.setInt(1, id);
                ResultSet rs = pstUpdate.executeQuery();

                // Print the updated student records
                while (rs.next()) {
                    System.out.println("id      : " + rs.getInt("id"));
                    System.out.println("name    : " + rs.getString("name"));
                    System.out.println("gender  : " + rs.getString("gender"));
                    System.out.println("email   : " + rs.getString("email"));
                    System.out.println("password: " + rs.getString("password") + "\n");
                }

                rs.close();
            } else {
                System.out.println("Failed to Update the Student Record!!!");
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (pstUpdate != null) {
                    pstUpdate.close();
                }
                if (stmtSelect != null) {
                    ((Connection) stmtSelect).close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}